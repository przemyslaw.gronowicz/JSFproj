/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sda.edu.bin;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author RENT
 */
//@Named(value = "jSFManagedBean")
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class JSFManagedBean {
    
    private final  EntityManagerFactory emf;
    private final EntityManager em;
    
    public JSFManagedBean(){
       emf = Persistence.createEntityManagerFactory("PU");
       em = emf.createEntityManager();
        
//        try{
//        emf = Persistence.createEntityManagerFactory("PU");
//        em = emf.createEntityManager();
//        }catch(Exception e){
//            e.printStackTrace();
//        }
        posts.add(new DTOPost("Pierwszy nowy post"));
        posts.add(new DTOPost("Drugi nowy post"));
        posts.add(new DTOPost("Trzeci nowy post"));
    }

    public EntityManager getEm() {
        return em;
    }
    
    
    private String version = "6.9";
    private List<DTOPost> posts = new ArrayList<DTOPost>();

    public List<DTOPost> getPosts() {
        return posts;
    }

    public String getVersion() {
        return version;
    }

    /**
     * Creates a new instance of JSFManagedBean
     */
}
